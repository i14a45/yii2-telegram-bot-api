<?php


namespace i14a45\botapi;


use i14a45\botapi\client\ApiClient;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class Api
 * @package i14a45\botapi
 *
 * @property ApiClient|object $client
 */
class Api extends Component
{
    /** @var string Authorization token */
    public $token;

    /** @var string Telegram bot API url */
    public $apiUrl = 'https://api.telegram.org';

    /** @var ApiClient */
    private $_client = ApiClient::class;

    public function getMe()
    {
        return $this->getClient()->makeRequest('getMe');
    }

    /**
     * @param ApiClient|array|string $client
     */
    public function setClient($client)
    {
        $this->_client = $client;
    }

    /**
     * @return ApiClient
     * @throws InvalidConfigException
     */
    public function getClient()
    {
        if (!is_object($this->_client)) {
            $this->_client = Yii::createObject($this->_client, [$this, $this->apiUrl, $this->token]);
        }

        return $this->_client;
    }

}