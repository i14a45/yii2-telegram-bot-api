<?php

namespace i14a45\botapi\models\request;

use yii\base\Model;

/**
 * Class SendMessage
 * @package i14a45\tgbot\models\request
 */
class SendMessage extends Model
{
    public $chat_id;

    public $text;

    public $parse_mode;

    public $entities;

    public $disable_web_page_preview;

    public $disable_notification;

    public $reply_to_message_id;

    public $allow_sending_without_reply;

    public $reply_markup;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['chat_id', 'text'], 'required'],
            [['text', 'parse_mode'], 'string'],
            [['disable_web_page_preview', 'disable_notification'], 'boolean'],
            [['reply_to_message_id'], 'integer'],
        ];
    }
}