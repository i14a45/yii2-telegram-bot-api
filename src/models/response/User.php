<?php


namespace i14a45\botapi\models\response;


use yii\base\Model;

class User extends Model
{
    public $id;
    public $is_bot;
    public $first_name;
    public $last_name;
    public $username;
    public $language_code;
    public $can_join_groups;
    public $can_read_all_group_messages;
    public $supports_inline_queries;
}
