<?php


namespace i14a45\botapi\client;


use i14a45\botapi\Api;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\HttpException;

/**
 * Class ApiClient
 * @package i14a45\botapi
 */
class ApiClient extends Component
{
    /** @var Api  */
    private $_api;

    /** @var string */
    private $_token;

    /** @var Client */
    private $_client;

    /**
     * constructor.
     * @param Api $api
     * @param string $apiUrl
     * @param string $token
     * @param array $config
     */
    public function __construct(Api $api, string $apiUrl, string $token, $config = [])
    {
        $this->_api = $api;
        $this->_token = $token;
        $this->_client = new Client(['baseUrl' => $apiUrl]);
        parent::__construct($config);
    }

    /**
     * @param string $method
     * @param Model|null $model
     * @return mixed
     * @throws InvalidConfigException
     * @throws Exception
     * @_throws ValidationException
     * @throws HttpException
     */
    public function makeRequest(string $method, ?Model $model = null)
    {
        if ($model && !$model->validate()) {
            //throw new ValidationException(Json::encode($model->errors));
        }

        try {
            $response = $this->_client->createRequest()
                ->setFormat(Client::FORMAT_JSON)
                ->setUrl('/bot' . $this->_token . '/' . $method)
                ->setMethod($this->method)
                ->setData($model->attributes)
                ->send();
        } catch (InvalidConfigException | Exception $e) {
            throw $e;
        }

        if (!$response->isOk) {
            throw new HttpException($response->statusCode, $response);
        }

        return $response->getData();
    }
}