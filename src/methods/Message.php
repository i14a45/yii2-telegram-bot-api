<?php

namespace i14a45\botapi\methods;

use i14a45\botapi\models\request\SendMessage;
use yii\base\Model;

/**
 * Trait Message
 * @package i14a45\botapi\methods
 *
 * @method mixed makeRequest(string $method, ?Model $model = null)
 */
trait Message
{
    public function sendMessage(SendMessage $message)
    {
        $client = $this->makeRequest('sendMessage', $message);
    }
}