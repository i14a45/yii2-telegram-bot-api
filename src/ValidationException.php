<?php


namespace i14a45\botapi;


use yii\base\Exception;

/**
 * Class ValidationException
 * @package i14a45\botapi
 */
class ValidationException extends Exception
{
    public function getName()
    {
        return 'Invalid data';
    }
}