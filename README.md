Telegram bot API for Yii2
=========================

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).
```
composer require i14a45/yii2-telegram-bot-api "dev-master"
```